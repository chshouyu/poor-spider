#!/usr/bin/env python
#-*- coding:utf-8 -*-
# 最简单的爬虫小程序

import urllib2
import utils

def main():
	print utils.getLine('开始抓取')

	target = 'http://github.com'

	fileName = target[7:]
	html = ''
	try:
		html = urllib2.urlopen(target).read()
	except:
		print 'Can not connect.'

	if html != '':
		f = file('htmls/' + fileName + '.html', 'w')

		f.write(html)
		f.close()
		print utils.getLine('抓取完毕')
		print '长度为：%s' % len(html)
		print '文件：%s' % fileName + '.html'
	

if __name__ == '__main__':
	main()